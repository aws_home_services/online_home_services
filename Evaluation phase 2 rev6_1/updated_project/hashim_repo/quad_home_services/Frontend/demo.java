class demo { //secret

  public static void main(String[] args) {
    String s = "MAYuR123";
    char ch[] = s.toCharArray();
    for (int i = 0; i < s.length(); i++) {
      if (Character.isUpperCase(ch[i])) {
        if (ch[i] == 'A') {
          ch[i] = '*';
        } else {
          ch[i] = Character.toLowerCase(ch[i]);
          ch[i] = --ch[i];
        }
      } else if (Character.isLowerCase(ch[i])) {
        if (ch[i] == 'z') {
          ch[i] = '@';
        } else {
          ch[i] = Character.toUpperCase(ch[i]);
          ch[i] = ++ch[i];
        }
      } else if (Character.isDigit(ch[i])) {
        if (ch[i] == 0) {
          ch[i] = '|';
        } else ch[i] = --ch[i];
      } else {
        if (ch[i] == ' ') ch[i] = '^'; else ch[i] = '~';
      }
    }

    for (int i = 0; i < s.length(); i++) {
      System.out.print(ch[i]);
    }
  }
}
//  class demo implements Runnable {
//   int x, y;
//   public void run() {
//     for (int i = 0; i < 1000; i++) synchronized (this) {
//       x = 12;
//       y = 12;
//     }
//     System.out.print(x + " " + y + " ");
//   }
//   public static void main(String args[]) {
//     demo run = new demo();
//     Thread t1 = new Thread(run);
//     Thread t2 = new Thread(run);
//     t1.start();
//     t2.start();
//   }
// }
