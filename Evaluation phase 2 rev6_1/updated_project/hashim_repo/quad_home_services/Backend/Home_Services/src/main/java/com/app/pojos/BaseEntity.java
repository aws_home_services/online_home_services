package com.app.pojos;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@MappedSuperclass //inheritance achieved parent class
// To tell hibernate , following is a base class for all other entities ,
					// containing common features BUT without any table associated with it
@Getter
@Setter
@ToString
public class BaseEntity {
	@Id //Specifies the primary key of an entity
	@GeneratedValue(strategy = GenerationType.IDENTITY)//auto increment
	//primary(self)-surrogate key(auto)
	private Long id;
}
